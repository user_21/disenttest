import "./modal.scss";

const Modal = ({ modalActive, setModalActive, activeCountry }) => {

    if (!activeCountry.name) {
        return
    }

    const closeModal = () => {
        setModalActive(false);
    }

    return (
        <div
            className={`modal ${modalActive ? "active" : ""}`}
            onClick={(e) => {
                e.stopPropagation();
                closeModal();
            }}
        >
            <div
                className="modal__content"
                onClick={(e) => {
                    e.stopPropagation();
                }}
            >
                <button
                    className="modal__close"
                    onClick={() => closeModal()}
                >
                </button>

                {activeCountry && (
                    <>
                        <div className="modal__title">
                            <img src={activeCountry.flags.png} alt={activeCountry.flags.alt} />
                            {activeCountry.name.official}
                        </div>
                        <table className="modal__table">
                            <tbody>
                                <tr>
                                    <th>Capital</th>
                                    <td>{activeCountry.capital}</td>
                                </tr>
                                <tr>
                                    <th>Region</th>
                                    <td>{activeCountry.region}</td>
                                </tr>
                                <tr>
                                    <th>Population</th>
                                    <td>{activeCountry.population}</td>
                                </tr>
                                <tr>
                                    <th>Area</th>
                                    <td>{activeCountry.area} km<sup>2</sup></td>
                                </tr>
                                <tr>
                                    <th>Languages</th>
                                    <td>{Object.values(activeCountry.languages).join(', ')}</td>
                                </tr>
                            </tbody>
                        </table>
                    </>
                )}
            </div>
        </div>
    )
}

export default Modal;