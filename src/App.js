import './App.scss';
import { useState, useEffect } from "react";
import Modal from './components/modal/Modal';

function App() {
    const [countries, setCountries] = useState([]);
    const [modalActive, setModalActive] = useState(false);
    const [activeCountry, setaActiveCountry] = useState({});

    const getCountries = async () => {
        try {
            const response = await fetch('https://restcountries.com/v3.1/all');

            if (!response.ok) {
                throw new Error('Failed to fetch data');
            }

            const data = await response.json();

            if (!data || data.length === 0) {
                setCountries(null);
            } else {
                const sortedCountries = data.sort((a, b) => (a.name.common > b.name.common ? 1 : -1));
                setCountries(sortedCountries);
            }
        } catch (error) {
            console.error("Error: ", error);
            setCountries(null);
        }
    };

    useEffect(() => {
        getCountries();
    }, []);

    const clickCountry = (country) => {
        setaActiveCountry(country);
        setModalActive(true);
    }

    return (
        <div className="main-page">
            <div className="main-page__content">
                {countries === null
                    ?
                    <div className="main-page__error">
                        Error loading data
                        <img src={require("./sad.png")} alt="sad smile" />
                    </div>
                    :
                    <ul className="main-page__list">
                        {countries.map((country, index) => {
                            return (
                                <li
                                    className="main-page__list-item"
                                    key={index}
                                    onClick={() => clickCountry(country)}
                                >
                                    <img src={country.flags.png} alt={country.flags.alt} />
                                    {country.name.common}
                                </li>
                            )
                        })}
                    </ul>
                }
            </div>
            <Modal modalActive={modalActive} setModalActive={setModalActive} activeCountry={activeCountry} />
        </div>
    );
}

export default App;
